﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PalmCoDevelopment.Startup))]
namespace PalmCoDevelopment
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
